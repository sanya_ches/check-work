# Onboarding on "Check Work" start-up

## How to start work in my project

1) Add access to Figma project (Install Figma-desktop or use web) https://www.figma.com/file/nCDfgbdKqBwM6qoU4HiOwG/Check-Work?node-id=0%3A1

2) Install python v3+, docker last-version, docker-compose last-version, node v12+ (last), yarn v1+(last)

3) Clone repository:
    - Add ssh-keys to gitlab (https://gitlab.com/profile/keys)
    - CMD `git clone git@gitlab.com:sanya_ches/check-work.git` in your project's directory
    - CMD `cd check-work`
    - CMD `docker-compose up`

4) Read project's concept https://docs.google.com/document/d/1dB0_2dymnb8xMVy3reMJoqBDxo6OldxcJ3jjgpSwfpM/edit?usp=sharing

5) ### Start work !

## Workflow

### Gitlab (Github) workflow

#### 1) Create issue if not already created
#### 2) Go to issue and click on "Create Merge Request"
#### 3) Switch to the branch and work.
#### 4) Make many many many commits :)
#### 5) Update the Changelog.md
#### 6) Click on Resolve WIP
#### 7) (Wait for approval)
#### 8) Click on Merge.

## Gitflow

### - Don't merge into Master, only for production
### - Merge your code into Dev branch